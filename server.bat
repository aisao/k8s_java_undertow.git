@echo off
setlocal & pushd

set base=%~dp0

rem 设置java环境变量
set JAVA_HOME=%java_bin%bin
set CLASS_PATH=.;%java_bin%lib\dt.jar;%java_bin%lib\tools.jar
rem --------------------------------------------------

set main=com.web.boot.Startup

set CP=%JAVA_HOME%;%CLASS_PATH%;%base%\lib\*;%base%\bin
title java_http

java -Xms64M -Xmx128M -Dfile.encoding=UTF-8 -Duser.timezone=GMT+8 -DserverPort=13000 -cp "%CP%" %main%

endlocal & popd

rem 等待任意键退出
echo [press any key to exit]

pause>nul