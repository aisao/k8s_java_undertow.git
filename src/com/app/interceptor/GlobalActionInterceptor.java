package com.app.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 全局过滤器
 * **/
public class GlobalActionInterceptor implements Interceptor
{

    @Override
    public void intercept(Invocation invocation)
    {
        invocation.invoke();
    }

}
