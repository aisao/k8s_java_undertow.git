package com.app.util.log;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogMsgFormat extends Formatter
{

	@Override
	public String format(LogRecord record) 
	{
		return record.getLevel()+":"+System.currentTimeMillis()+" "+record.getMessage()+System.getProperty("line.separator");
	}

}